; Drush make file for PlexityNet OpenAtrium 6.x multisites.
api = "2"


; Core
; ----

; Build process taken from
; http://cgit.drupalcode.org/openatrium/plain/drupal-org-core.make?id=refs/heads;id2=6.x-1.x
core = "6.x"
projects[drupal][type] = "core"
projects[drupal][version] = "6.38"
projects[drupal][patch][] = "https://www.drupal.org/files/robots_txt_rollback.patch"


; Distribution / Install profile
; ------------------------------

; Build process taken from
; http://cgit.drupalcode.org/openatrium/plain/build-openatrium.make?id=refs/heads/6.x-1.x
; Download the OpenAtrium Install profile and recursively build all its
; dependencies:
projects[openatrium][type] = "profile"
projects[openatrium][download][type] = "git"
projects[openatrium][download][url] = "git://git.drupal.org/project/openatrium.git"
projects[openatrium][download][tag] = "6.x-1.9"
projects[openatrium][patch][2178419] = "https://www.drupal.org/files/issues/2178419-44-oa_security_updates.patch"


; Contrib modules
; ---------------

; Contrib modules for PlexityNet.

projects[persistent_login][type] = "module"
projects[persistent_login][subdir] = "contrib"
projects[persistent_login][version] = "1.4"

